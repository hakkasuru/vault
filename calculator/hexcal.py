#!/bin/sh

__version__ = 1.1

import argparse
import re
import sys

def main():

	parser = argparse.ArgumentParser(prog='hexcal.py',usage='%(prog)s [options]', description='Basic hexadecimal calculator')
	parser.add_argument('-d', '--decimal', help='caculates hexadecimal values for decimals', type=int, dest='dvar')
	parser.add_argument('-s', '--string', help='caculates the hexadecimal values for strings', type=str, dest='svar')
	parser.add_argument('-hd', '--hextodec', help='caculates the decimal values for hexadecimals', type=str, dest='hdvar')
	parser.add_argument('-hs', '--hextostring', help='converts to string from hexadecimals', type=str, dest='hsvar')
	parser.add_argument('-ex', '--encodehex', help='adds \\x to hex encoded strings', type=str, dest='exvar')

	args = parser.parse_args()

	if len(sys.argv) < 2:
		parser.print_help()
		sys.exit(0)

	if args.dvar:

		if 'L' in hex(args.dvar)[2:]:
			print "out of range"
		else:
			print hex(args.dvar)[2:]

	if args.svar:

		hexlist = []

		for char in list(args.svar):
			hexlist.append(char.encode('hex'))

		print 'string	   = ' + str(list(args.svar))
		print 'hex		  = ' + str(hexlist)
		print 'hex(joined)  = ' + ''.join(hexlist)

	if args.hdvar:

		if len(re.findall('[g-zG-Z]', args.hdvar)) == 0:
			print int('0x'+args.hdvar.lower(), 16)
		else:
			print 'Invalid characters: ' + ','.join(re.findall('[g-zG-Z]', args.hdvar))

	if args.hsvar:

		if len(args.hsvar) % 2 == 0:
			if len(re.findall('[g-zG-Z]', args.hsvar)) == 0:
				hexlist = re.findall('..?', args.hsvar)
				count = 0
				for char in hexlist:
					hexlist[count] = char.lower().decode('hex')
					count = count + 1
				print ''.join(hexlist)
			else:
				print 'Invalid characters: ' + ','.join(re.findall('[g-zG-Z]', args.hsvar))
		else:
			print 'odd number of characters'

	if args.exvar:

		hexlist = []

		for i in range(len(list(args.exvar))):
			if i % 2 == 1:
				hexlist.append(list(args.exvar)[i-1]+list(args.exvar)[i])

		print '\\x' + '\\x'.join(hexlist)

if __name__ == "__main__":
	main()
